<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 4/6/14
 * Time: 2:52 PM
 */
class Mena_Openerp_Helper_Data extends Mage_Core_Helper_Abstract
{
    public static $MENA_FULFILLMENT = 'mena.api.fulfilment';

    protected $baseURL;
    protected $userName;
    protected $password;
    protected $dbName;

    //used non-login calls (basically, execute)
    protected $userId;

    protected $connections = array();

    /**
     *  Initialize Open ERP Connection Parameters
     */
    public function init() {

        $this->baseURL = Mage::getStoreConfig('openerp/settings/baseurl');
        $this->userName = Mage::getStoreConfig('openerp/settings/username');
        $this->password = Mage::getStoreConfig('openerp/settings/password');
        $this->dbName = Mage::getStoreConfig('openerp/settings/database');

    }

    /**
     * Wrapper Function Connect
     * @throws Exception
     */
    public function connect() {

        $zendDir = Mage::getBaseDir().'/lib/Zend/XmlRpc/Client.php';
        require_once ($zendDir);

        $this->init();
        if( (!isset($this->connections['common'])) || ( !isset($this->connections['object'])) ) {
            try {

                $this->connections['common'] = new Zend_XmlRpc_Client($this->baseURL.'/common');
                $this->connections['common']->getHttpClient()->setConfig(array('timeout' => 60));

                $this->connections['object'] = new Zend_XmlRpc_Client($this->baseURL.'/object');
                $this->connections['object']->getHttpClient()->setConfig(array('timeout' => 60));

                $this->userId = $this->connections['common']->call("login", array($this->dbName,
                    $this->userName, $this->password));

            } catch( Execption $e ) {
                print_r($e->getMessage());
                throw new Exception( "Could not connect to openerp service" );
            }
        }
    }

    /**
     *
     * Wrapper Function Open ERP API Call
     *
     * @param string $model
     * @param string $method
     * @param array $params
     * @return bool
     */
    public function call( $model, $method, $params ) {

        $this->connect();
        $args = array($this->dbName, $this->userId,$this->password,
            $model,$method, $params );

        try {
            $return = $this->connections['object']->call('execute', $args);
        } catch( Exception $e ) {
            echo (string)$e->getMessage();
        }

        return $return;
    }
}