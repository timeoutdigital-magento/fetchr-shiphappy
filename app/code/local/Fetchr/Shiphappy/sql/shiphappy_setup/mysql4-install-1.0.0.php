<?php

$installer = $this;

$installer->startSetup();

// die('test!');

// Required tables
$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');

try {
    // Insert statuses
    $installer->getConnection()->insertArray(
        $statusTable,
        array(
            'status',
            'label'
        ),
        array(
            array('status' => 'fetchr_shipping', 'label' => 'Fetchr Shipping'),
            array('status' => 'fetchr_delivery_scheduled', 'label' => 'Delivery Scheduled'),
            array('status' => 'fetchr_scheduled_for_delivery', 'label' => 'Scheduled for Delivery'),
            array('status' => 'fetchr_hold', 'label' => 'Held at Fetchr'),
            array('status' => 'fetchr_order_dispatched', 'label' => 'In Transit'),
            array('status' => 'fetchr_ship', 'label' => 'Ready for Pick up')
        )
    );

    // Insert states and mapping of statuses to states
    $installer->getConnection()->insertArray(
        $statusStateTable,
        array(
            'status',
            'state',
            'is_default'
        ),
        array(
            array(
                'status' => 'fetchr_shipping',
                'state' => 'processing',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_delivery_scheduled',
                'state' => 'processing',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_scheduled_for_delivery',
                'state' => 'processing',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_hold',
                'state' => 'processing',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_order_dispatched',
                'state' => 'processing',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_ship',
                'state' => 'processing',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_ship',
                'state' => 'new',
                'is_default' => 0
            ),
            array(
                'status' => 'fetchr_delivered',
                'state' => 'new',
                'is_default' => 0
            )
        )
    );
} catch(Exception $e) {
    Mage::log('Something went wrong while trying to install and map order statuses: ' . $e->getMessage(), null, 'fetchr.log', true);
}

$installer->endSetup();

