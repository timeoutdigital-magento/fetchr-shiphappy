<?php

/**
  * Fetchr.
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Open Software License (OSL 3.0)
  * It is also available through the world-wide-web at this URL:
  * https://fetchr.zendesk.com/hc/en-us/categories/200522821-Downloads
  * If you did not receive a copy of the license and are unable to
  * obtain it through the world-wide-web, please send an email
  * to ws@fetchr.us so we can send you a copy immediately.
  *
  * DISCLAIMER
  *
  * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
  * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Shiphappy) for your
  * needs please refer to http://www.fetchr.us for more information.
  *
  * @author     Danish Kamal
  * @copyright  Copyright (c) 2015 Fetchr (http://www.fetchr.us)
  * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
  */
class Fetchr_Shiphappy_Model_Ship_Bulkstatus
{
  public function run($force_order_update=false)
    {
      if(!Mage::getStoreConfig('shiphappy/settings/enabled'))
            return;
        if(!$force_order_update) {
            if(Mage::getStoreConfig('shiphappy/settings/order_push') == '')
                return;
        }
        $status_mapping = array(
             'Order Created' => 'fetchr_shipping',
             'Scheduled for delivery' => 'fetchr_scheduled_for_delivery',
             'Order dispatched' => 'fetchr_order_dispatched',
             'Returned to Client' => 'fetchr_returned',
             'Customer care On hold' => 'fetchr_hold',
             'Delivered' => 'fetchr_delivered',
             'Order Created'=>'fetchr_pending',
         );
        $this->userName = Mage::getStoreConfig('shiphappy/settings/username');
        $this->password = Mage::getStoreConfig('shiphappy/settings/password');

        $collection   =   Mage::getModel('sales/order')->getCollection()->addFieldToFilter('main_table.status', array(
                      array(
                          'in' => array(
                              'fetchr_shipping',
                              'fetchr_scheduled_for_delivery',
                              'fetchr_order_dispatched',
                              'fetchr_hold'
                          ),
                      ),
                  ));

        $result = array();
        $tracking_numbers = array();
        if ($collection->getData()) {
            //echo "<pre>";print_r($collection->getData());die;
            foreach ($collection as $value) {
                $order = Mage::getModel('sales/order')->load($value->getId());
                
                foreach($order->getShipmentsCollection() as $shipment) {
                    foreach($shipment->getAllTracks() as $tracknum) {
                      //echo $tracknum->getNumber().'<br />';
                      $tracking_numbers[] = $tracknum->getNumber();
                    }
                }
            }
        }
        //echo "<pre>";print_r($tracking_numbers);die("hi");

      $data   =   array(  'username' => $this->userName,
              'password' => $this->password,
              'method' => 'get_status_bulk',
              'data' =>  $tracking_numbers
              );
    
    $data_string = json_encode($data) ;
    //echo "<pre>";print_r($data_string);die;
    $ch = curl_init("http://dev.menavip.com/api/get-status/");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    $results = curl_exec($ch);
    
    $results = json_decode($results);
    Mage::log('Current status: ' . $result->package_state, null, 'fetchr.log');

    foreach ($collection as $value) {

            $currentOrder = Mage::getModel('sales/order')->load($value->getId());

      foreach ($results->response as $result) {
        $currentStatus  = $result->package_state;

        foreach ($status_mapping as $key => $value) {

                  if (strstr($currentStatus, $key)) {
                      if (strstr($value, $currentOrder->getStatus())) {
                          break;
                      }
                      if($value == 'fetchr_shipping'){
                          break;
                      }
                      if(strstr($currentStatus, 'Delivered')){

              //$invoice = $currentOrder->getInvoiceCollection();
                      //if ($currentOrder->hasInvoices() == 1) {
                          //$invoice = Mage::getModel('sales/order_invoice')->load($currentOrder->getIncrementId());
                          foreach ($currentOrder->getInvoiceCollection() as $inv) {
                              $inv->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID)->save();
                          }
                      // }else{
                      //  $invoice = Mage::getModel('sales/service_order', $currentOrder)->prepareInvoice();
                   //              $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                   //              $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID)->save();
                   //              $invoice->register();

                   //              //$invoice->getOrder()->setStatus('fetchr_shipping');
                   //              $transactionSave = Mage::getModel('core/resource_transaction')
                   //              ->addObject($invoice);
                   //              $transactionSave->save();
                   //          }
            }
            $currentOrder->setStatus($value)->save();
                      $currentOrder->addStatusHistoryComment($currentStatus, false)->save();
                  }
              }
      }
    }
    return $results;

    }
}