<?php

/**
  * Fetchr.
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Open Software License (OSL 3.0)
  * It is also available through the world-wide-web at this URL:
  * https://fetchr.zendesk.com/hc/en-us/categories/200522821-Downloads
  * If you did not receive a copy of the license and are unable to
  * obtain it through the world-wide-web, please send an email
  * to ws@fetchr.us so we can send you a copy immediately.
  *
  * DISCLAIMER
  *
  * Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
  * versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Shiphappy) for your
  * needs please refer to http://www.fetchr.us for more information.
  *
  * @author     Danish Kamal
  * @copyright  Copyright (c) 2015 Fetchr (http://www.fetchr.us)
  * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
  */
 class Fetchr_Shiphappy_Model_Ship_Getstatus
 {
     public function run($force_order_update=false)
     {
         if(!Mage::getStoreConfig('shiphappy/settings/enabled'))
             return;
         if(!$force_order_update) {
             if(Mage::getStoreConfig('shiphappy/settings/order_push') == '')
                 return;
         }
         $status_mapping = array(
             'Order Created' => 'fetchr_shipping',
             'Scheduled for delivery' => 'fetchr_scheduled_for_delivery',
             'Order dispatched' => 'fetchr_order_dispatched',
             'Returned to Client' => 'fetchr_returned',
             'Customer care On hold' => 'fetchr_hold',
             'Delivered' => 'fetchr_delivered',
         );
         $collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('main_table.status', array(
             array(
                 'in' => array(
                     'fetchr_shipping',
                     'fetchr_scheduled_for_delivery',
                     'fetchr_order_dispatched',
                     //'fetchr_returned',
                     'fetchr_hold'
                     //'fetchr_delivered',
                     //'processing',
                     //'complete'
                 ),
             ),
         ));
         $result = array();
         if ($collection->getData()) {
          //echo "<pre>";print_r($collection->getData());die;
             foreach ($collection as $value) {
                 $order = Mage::getModel('sales/order')->load($value->getId());
                 $this->userName = Mage::getStoreConfig('shiphappy/settings/username');
                 $this->password = Mage::getStoreConfig('shiphappy/settings/password');

                 $tracking_numbers = array();
                 $tracking_number = null;
                 foreach($order->getShipmentsCollection() as $shipment)
                 {
                     foreach($shipment->getAllTracks() as $tracknum)
                     {
                         $tracking_numbers[] = $tracknum->getNumber();
                         $tracking_number = $tracknum->getNumber();
                     }
                 }
                //  $tracking_number = end($tracking_numbers);

                 $data = array(
                           'username' => $this->userName,
                           'password' => $this->password,
                           'method' => 'get_status',
                           'data' => $tracking_number,
                           );
                 $data_string = 'args='.json_encode($data);
                 $ch = curl_init('http://dev.menavip.com/api/');
                 curl_setopt($ch, CURLOPT_POST, true);
                 curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 $response = curl_exec($ch);

                 $result[] = array(
                     'order_number' => $order->getIncrementId(),
                     'order_tracking_number' => $tracking_number,
                     'curl_response' => strip_tags($response)
                 );

                 //echo "<pre>";print_r($result);
                 Mage::log('Order #' . $order->getIncrementId() . ' status result: ' . print_r($response, true), null, 'fetchr.log');
                 
                 $json = json_decode($response);
                 // if($order->getIncrementId() == '100000080'){
                 //  $currentStatus = 'Delivered';
                 // }else{
                  $currentStatus = $json->Status;
                 //}
                 //echo $currentStatus.'<br />';
                 Mage::log('Current status: ' . $currentStatus, null, 'fetchr.log');
                 foreach ($status_mapping as $key => $value) {
                     if (strstr($currentStatus, $key)) {
                         if (strstr($value, $order->getStatus())) {
                            break;
                         }
                         if($value == 'fetchr_shipping'){
                            break;
                         }
                         //var_dump($order->hasInvoices());
                         if($currentStatus == 'Delivered'){
                          //$invoice = $order->getInvoiceCollection();
                          if ($order->hasInvoices() == 1) {
                              //die("hoan");
                              $invoice = Mage::getModel('sales/order_invoice')->load($order->getIncrementId());
                              
                              foreach ($order->getInvoiceCollection() as $inv) {
                                $inv->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID)->save();
                              }

                          }else{
                                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                                $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID)->save();
                                $invoice->register();

                                //$invoice->getOrder()->setStatus('fetchr_shipping');
                                $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice);
                                $transactionSave->save();
                                
                            }
                        }
                        $order->setStatus($value)->save();
                        $order->addStatusHistoryComment($currentStatus, false)->save();
                     }
                 }
             }
             //echo "<pre>";print_r($result);die;
             return $result;
         } else {
             return array(
                 'status' => 'success',
                 'message' => 'nothing needs to be updated, collection is empty!'
             );
         }
     }
 }