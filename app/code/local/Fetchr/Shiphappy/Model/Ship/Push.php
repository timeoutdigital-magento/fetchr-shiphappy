<?php
/**
* Fetchr.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* It is also available through the world-wide-web at this URL:
* https://fetchr.zendesk.com/hc/en-us/categories/200522821-Downloads
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to ws@fetchr.us so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Fetchr Magento Extension to newer
* versions in the future. If you wish to customize Fetchr Magento Extension (Fetchr Shiphappy) for your
* needs please refer to http://www.fetchr.us for more information.
*
* @author     Danish Kamal
* @copyright  Copyright (c) 2015 Fetchr (http://www.fetchr.us)
* @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
class Fetchr_Shiphappy_Model_Ship_Push
{
    public function run($force_push=false)
    {
        // if(!Mage::getStoreConfig('shiphappy/settings/enabled'))
        //     return;
        if(!$force_push) {
            if(Mage::getStoreConfig('shiphappy/settings/order_status_update') == '')
                return;
        }
        Mage::log('Push operation initiated!', null, 'fetchr.log');
        $result = array();
        // $collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('main_table.status', array(
        //     array(
        //         'in' => array(
        //             'fetchr_ship',
        //         ),
        //     ),
        // ));
        $collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', array('in' => array('fetchr_ship')));
        $store = Mage::app()->getStore();
        $storeTelephone = Mage::getStoreConfig('general/store_information/phone');
        $storeAddress = Mage::getStoreConfig('general/store_information/address');
        if ($collection->getData()) {
            $resource = Mage::getSingleton('core/resource');
            $adapter = $resource->getConnection('core_read');
            try {
                foreach ($collection as $value) {
                    $order = Mage::getModel('sales/order')->load($value->getId());
                    // check if the module is enabled for a particular store
                    if(!Mage::getStoreConfig('shiphappy/settings/enabled', $order->getStoreId()))
                        continue;
                    $paymentType = $order->getPayment()->getMethodInstance()->getId();
                    $paymentType = $order->getPayment()->getMethodInstance()->getCode();
                    // Get Items Ordered Name
                    foreach ($order->getAllItems() as $item) {
                        if ($item['product_type'] == 'configurable') {
                            $itemArray[] = array(
                                'client_ref' => $order->getIncrementId(),
                                'name' => $item['name'],
                                'sku' => $item['sku'],
                                'quantity' => $item['qty_ordered'],
                                'merchant_details' => array(
                                    'mobile' => $storeTelephone,
                                    'phone' => $storeTelephone,
                                    'name' => $store->getFrontendName(),
                                    'address' => $storeAddress,
                                ),
                                'COD' => $order->getShippingAmount(),
                                'price' => $item['price'],
                                'is_voucher' => 'No',
                            );
                            break;
                        } else {
                            $itemArray[] = array(
                                'client_ref' => $order->getIncrementId(),
                                'name' => $item['name'],
                                'sku' => $item['sku'],
                                'quantity' => $item['qty_ordered'],
                                'merchant_details' => array(
                                    'mobile' => $storeTelephone,
                                    'phone' => $storeTelephone,
                                    'name' => $store->getFrontendName(),
                                    'address' => $storeAddress,
                                ),
                                'COD' => $order->getShippingAmount(),
                                'price' => $item['price'],
                                'is_voucher' => 'No',
                            );
                        }
                    }
                    //echo "<pre>";print_r($itemArray);die;
                    $discountAmount = 0;
                    if ($order->getDiscountAmount()) {
                        $discountAmount = abs($order->getDiscountAmount());
                    }
                    //print_r($paymentType);die("why");
                    $address = $order->getShippingAddress()->getData();
                    switch ($paymentType) {
                        case 'cashondelivery':
                        case 'phoenix_cashondelivery':
                            $paymentType    = 'COD';
                            $grandtotal     = $order->getGrandTotal();
                            $discount       = $discountAmount;
                        break;
                        case 'ccsave':
                            $paymentType    = 'CCOD';
                            $grandtotal     = $order->getGrandTotal();
                            $discount       = $discountAmount;
                        break;
                        default:
                            $paymentType    = 'cd';
                            $grandtotal     = $order->getGrandTotal();
                            $discount       = $discountAmount;
                        break;
                    }

                    // $paymentType = 'cd';
                    // $grandtotal = 0;//$order->getGrandTotal();
                    // $discount = 0;//$discountAmount;

                    $this->serviceType = Mage::getStoreConfig('shiphappy/settings/servicetype');
                    $this->userName = Mage::getStoreConfig('shiphappy/settings/username');
                    $this->password = Mage::getStoreConfig('shiphappy/settings/password');
                    $ServiceType = $this->serviceType;
                    switch ($ServiceType) {
                        case 'fulfilment':
                        $dataErp[] = array(
                            'order' => array(
                                'items' => $itemArray,
                                'details' => array(
                                    'status' => '',
                                    'discount' => $discount,
                                    'grand_total' => $grandtotal,
                                    'customer_email' => Mage::getStoreConfig('shiphappy/settings/include_customer_email') ? $order->getCustomerEmail() : Mage::getStoreConfig('shiphappy/settings/custom_order_email'),
                                    'order_id' => $order->getIncrementId(),
                                    'customer_firstname' => $address['firstname'],
                                    'payment_method' => $paymentType,
                                    'customer_mobile' => $address['telephone'],
                                    'customer_lastname' => $address['lastname'],
                                    'order_country' => $address['country_id'],
                                    'order_address' => $address['street'].', '.$address['city'].', '.$address['country_id'],
                                ),
                            ),
                        );
                        break;
                        case 'delivery':
                        $dataErp = array(
                            'username' => $this->userName,
                            'password' => $this->password,
                            'method' => 'create_orders',
                            'pickup_location' => $storeAddress,
                            'data' => array(
                                array(
                                    'order_reference' => $order->getIncrementId(),
                                    'name' => $address['firstname'].' '.$address['lastname'],
                                    'email' => $order->getCustomerEmail(),//Mage::getStoreConfig('shiphappy/settings/include_customer_email') ? $order->getCustomerEmail() : Mage::getStoreConfig('shiphappy/settings/custom_order_email'),
                                    'phone_number' => $address['telephone'],
                                    'address' => $address['street'],
                                    'city' => $address['city'],
                                    'payment_type' => $paymentType,
                                    'amount' => $grandtotal,
                                    'description' => 'No',
                                    'comments' => 'No',
                                ),
                            ),
                        );
                    }
                    //echo "<pre>";print_r($dataErp);die;
                    $result[$order->getIncrementId()]['request_data'] = $dataErp;
                    $result[$order->getIncrementId()]['response_data'] = $this->_sendDataToErp($dataErp, $order->getIncrementId());
                    //print_r($result[$order->getIncrementId()]['response_data']);die("Honak");
                    if($dataErp['data'][0]['payment_type'] == 'COD' && $result[$order->getIncrementId()]['response_data'] != null){
                        try {
                                if(!$order->canInvoice())
                                {
                                    Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
                                }
                                 
                                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                                 
                                if (!$invoice->getTotalQty()) {
                                    Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
                                }
                                //var_dump($order->hasInvoices());
                                if($order->hasInvoices() == 1){
                                    $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                                    $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN)->save();
                                    $invoice->register();

                                    //$invoice->getOrder()->setStatus('fetchr_shipping');
                                    $transactionSave = Mage::getModel('core/resource_transaction')
                                    ->addObject($invoice);
                                    $transactionSave->save();
                                }
                            }
                            catch (Mage_Core_Exception $e) {
                             
                            }
                    }
                    unset($dataErp, $itemArray);
                }
            } catch (Exception $e) {
                echo (string) $e->getMessage();
            }
            return $result;
        }
    }
    protected function _sendDataToErp($data, $orderId)
    {
        $response = null;
        
        try {
            $this->accountType = Mage::getStoreConfig('shiphappy/settings/accounttype');
            $this->serviceType = Mage::getStoreConfig('shiphappy/settings/servicetype');
            $this->userName = Mage::getStoreConfig('shiphappy/settings/username');
            $this->password = Mage::getStoreConfig('shiphappy/settings/password');
            $ServiceType = $this->serviceType;
            $accountType = $this->accountType;
            switch ($accountType) {
                case 'live':
                $baseurl = Mage::getStoreConfig('shiphappy/settings/liveurl');
                break;
                case 'staging':
                $baseurl = Mage::getStoreConfig('shiphappy/settings/stagingurl');
            }
            switch ($ServiceType) {
                case 'fulfilment':
                $ERPdata = 'ERPdata='.json_encode($data);
                $ch = curl_init();
                $url = $baseurl.'/client/gapicurl/';
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $ERPdata.'&erpuser='.$this->userName.'&erppassword='.$this->password.'&merchant_name='.$this->userName);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);
                // validate response
                $decoded_response = json_decode($response, true);
                if(!is_array($decoded_response))
                    return $response;
                if ($response['response']['awb'] == 'SKU not found') {
                    $store = Mage::app()->getStore();
                    $cname = $store->getFrontendName();
                    $ch = curl_init();
                    $url = 'http://www.menavip.com/custom/smail.php';
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, 'orderId='.$orderId.'&cname='.$cname);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $output = curl_exec($ch);
                    curl_close($ch);
                }
                $comments = '';
                if ($response['response']['tracking_no'] != '0') {
                    $o_status = 'fetchr_shipping';
                    $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                    $order->setStatus($o_status)->setResponseMessage('Confirmation Received from Fetchr')->save();
                    $comments .= '<strong>Tracking URL:</strong> http://track.menavip.com/track.php?tracking_number='.$response['response']['tracking_no'];
                    $order->addStatusHistoryComment($comments, false)->save();
                    $sorderId = $orderId;
                    $order_data = Mage::getModel('sales/order')->loadByIncrementId($sorderId);
                    $items = $order_data->getAllVisibleItems();

                    $qty = array();

                    foreach ($items as $item) {
                        $product_id = $item->getProductId();
                        $Itemqty = $item->getQtyOrdered() - $item->getQtyShipped() - $item->getQtyRefunded() - $item->getQtyCanceled();
                        $qty[$item->getId()] = $Itemqty;
                    }

                    if ($order_data->canShip()) {
                        // $itemQty =  $order_data->getItemsCollection()->count();
                        $shipment = $order_data->prepareShipment($qty);
                        $trackdata = array();
                        $trackdata['carrier_code'] = 'Fetchr';
                        $trackdata['title'] = 'Fetchr';
                        $trackdata['number'] = $response['response']['tracking_no'];
                        $track = Mage::getModel('sales/order_shipment_track')->addData($trackdata);
                        $shipment->addTrack($track);
                        $shipment->register();
                        $shipment->getOrder()->setIsInProcess(true);
                        $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($shipment)
                        ->addObject($shipment->getOrder())
                        ->save();
                    }

                    return $response;
                }
                break;
                case 'delivery':
                
                $data_string = 'args='.json_encode($data);
                $ch = curl_init();
                $url = $baseurl.'/client/api/';
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);
                // validate response
                $decoded_response = json_decode($response, true);
                //print_r($response);
                if(!is_array($decoded_response))
                    return $response;
                $varshipID = $response;
                $shipidexpStr = explode('status', $varshipID);
                $ResValShip = $shipidexpStr[0];
                $RevArray = array(
                    '"',
                    '{',
                    );
                    $varshipIDTrim = str_replace($RevArray, '', $ResValShip);
                    $datas = rtrim($varshipIDTrim, ',');
                    $array1 = explode(',', $datas);
                    foreach ($array1 as $val) {
                        $array2 = explode(':', $val);
                        $comments = '';
                        if ($array2['1'] != '') {
                            $o_status = 'fetchr_shipping';
                            $order = Mage::getModel('sales/order')->loadByIncrementId($array2['0']);
                            $order->setStatus($o_status)->setResponseMessage('Confirmation Received from Fetchr')->save();
                            $comments .= '<strong>Tracking URL:</strong> http://track.menavip.com/track.php?tracking_number='.$array2['1'];
                            $order->addStatusHistoryComment($comments, false)->save();
                            $sorderId = $array2['0'];
                            $order_data = Mage::getModel('sales/order')->loadByIncrementId($sorderId);
                            $items = $order_data->getAllVisibleItems();

                            $qty = array();

                            foreach ($items as $item) {
                                $product_id = $item->getProductId();
                                $Itemqty = $item->getQtyOrdered() - $item->getQtyShipped() - $item->getQtyRefunded() - $item->getQtyCanceled();
                                $qty[$item->getId()] = $Itemqty;
                            }

                            if ($order_data->canShip()) {
                                // $itemQty =  $order_data->getItemsCollection()->count();
                                $shipment = $order_data->prepareShipment($qty);
                                $trackdata = array();
                                $trackdata['carrier_code'] = 'Fetchr';
                                $trackdata['title'] = 'Fetchr';
                                $trackdata['number'] = $array2['1'];
                                $track = Mage::getModel('sales/order_shipment_track')->addData($trackdata);
                                $shipment->addTrack($track);
                                $shipment->register();
                                $shipment->getOrder()->setStatus('fetchr_shipping');
                                $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($shipment)
                                ->addObject($shipment->getOrder())
                                ->save();

                                Mage::log('Order '.$orderId.' has been shipped!', null, 'fetchr.log');
                            } else {
                                Mage::log('Order '.$orderId.' cannot be shipped!', null, 'fetchr.log');
                            }

                            Mage::log('Order '.$orderId.' has been pushed!', null, 'fetchr.log');
                            Mage::log('Order data: '.print_r($data, true), null, 'fetchr.log');
                        }
                    }
                }
            } catch (Exception $e) {
                echo (string) $e->getMessage();
            }

            return $response;
        }
    }
